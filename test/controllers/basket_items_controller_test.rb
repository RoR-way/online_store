require 'test_helper'

class BasketItemsControllerTest < ActionDispatch::IntegrationTest

  def setup
    Product.create(id: 1)
  end

  test "should increase basket items when we add a new one" do
    assert_difference 'BasketItem.count' do
      post basket_items_path, params: 
      { basket_item: 
        { quantity:  1,
          product_id: 1
        } 
      }
    end
  end
  
  test "should update quantity if basket item exists" do
    post basket_items_path, params: 
      { basket_item: 
        { quantity:  1,
          product_id: 1
        } 
      }

    assert_difference 'BasketItem.last.quantity' do
      post basket_items_path, params: 
      { basket_item: 
        { quantity:  1,
          product_id: 1
        } 
      }
    end
  end
end
