require 'test_helper'

class BasketScenariosTest < ActionDispatch::IntegrationTest
  test "first scenario" do
    basket = baskets(:one)
    assert_equal "66.4", basket.total_after_discount.to_s
  end

  test "second scenario" do
    basket = baskets(:two)
    assert_equal "80.25", basket.total_after_discount.to_s
  end

  test "third scenario" do
    basket = baskets(:three)
    assert_equal "71.63", basket.total_after_discount.to_s
  end
end
