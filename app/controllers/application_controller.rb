class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def current_basket
    return @current_basket if defined?(@current_basket)
    session_basket
  end

  private

  def session_basket
    if cookies['basket_token'] && !Basket.find_by_token(cookies['basket_token']).nil?
      current_basket = Basket.find_by_token(cookies['basket_token'])
    else
      current_basket = Basket.create
      cookies['basket_token'] = current_basket.token
    end
    current_basket
  end
end
