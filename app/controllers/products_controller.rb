class ProductsController < ApplicationController

  def index
    @current_basket = current_basket
    @products = Product.all
  end

  def show
    @current_basket = current_basket
    @product = Product.find(params[:id])
    @basket_item = BasketItem.new
  end
end
