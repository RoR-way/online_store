class BasketItemsController < ApplicationController

  def create
    product = Product.find(basket_item_params[:product_id])

    if current_basket.basket_items.find_by_product_id(product.id).nil?
      basket_item = current_basket.basket_items.build(basket_item_params)
      basket_item.price = product.price
      basket_item.vat_rate_id = product.vat_rate_id
    else
      basket_item = current_basket.basket_items.find_by_product_id(product.id)
      basket_item.quantity += basket_item_params[:quantity].to_i
    end

    if basket_item.save
      redirect_to basket_path(current_basket.id)
    else
      redirect_to products_path
    end
  end

  private

  def basket_item_params
    params.require(:basket_item).permit(:quantity, :product_id)
  end
end
