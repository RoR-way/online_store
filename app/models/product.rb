# == Schema Information
#
# Table name: products
#
#  id          :integer          not null, primary key
#  name        :string
#  price       :decimal(, )
#  vat_rate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  deal_id     :integer
#

class Product < ApplicationRecord
  belongs_to :vat_rate
  belongs_to :deal
end
