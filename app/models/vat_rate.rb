# == Schema Information
#
# Table name: vat_rates
#
#  id         :integer          not null, primary key
#  name       :string
#  rate       :decimal(, )
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class VatRate < ApplicationRecord
  has_many :products
end
