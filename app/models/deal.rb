# == Schema Information
#
# Table name: deals
#
#  id              :integer          not null, primary key
#  overall         :decimal(, )
#  discount_rate   :float
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  deal_type       :string
#  quantity        :integer
#  discount_amount :decimal(, )
#

class Deal < ApplicationRecord

  def self.discount(item)
    discount = 0.0
    product = item.product
    if qualifies?(item, product)
      discount += calculate_discount(item, product)
    end

    return discount
  end

  def self.overall_discount(total)
    # replace equal operator with == in order to work on sqlite
    discount = Deal.where(["deal_type = 'total' AND overall <= ?", total]).first
    return discount.discount_rate if !discount.nil?
    return 0.0
  end

  private

  def self.calculate_discount(item, product)
    (item.quantity / product.deal.quantity) * product.deal.discount_amount
  end

  def self.qualifies?(item, product)
    return false if !defined?(product.deal)
    return true if !product.deal.nil? && item.quantity >= product.deal.quantity
  end


end
