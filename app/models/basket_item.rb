# == Schema Information
#
# Table name: basket_items
#
#  id          :integer          not null, primary key
#  basket_id   :integer
#  quantity    :integer
#  price       :decimal(, )
#  vat_rate_id :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  product_id  :integer
#

class BasketItem < ApplicationRecord
  belongs_to :product
  belongs_to :basket
  belongs_to :vat_rate

  def total
    (self.quantity * self.price) - Deal.discount(self)
  end

  def final_price
    self.total + (self.total * self.vat_rate.rate) / 100
  end

end
