# == Schema Information
#
# Table name: baskets
#
#  id         :integer          not null, primary key
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  token      :string
#

class Basket < ApplicationRecord
  has_many :basket_items

  has_secure_token

  def total_without_vat
    self.basket_items.map(&:total).sum
  end

  def total_with_vat
    self.basket_items.map(&:final_price).sum
  end

  def total_after_discount
    (total_with_vat - discount_amount).round(2)
  end

  private

  def discount_amount
    (total_with_vat * Deal.overall_discount(self.total_with_vat)) / 100
  end
end
