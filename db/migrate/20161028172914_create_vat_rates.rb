class CreateVatRates < ActiveRecord::Migration[5.0]
  def change
    create_table :vat_rates do |t|
      t.string :name
      t.decimal :rate

      t.timestamps
    end
  end
end
