class CreateDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :deals do |t|
      t.decimal :overall
      t.float :discount_rate
      t.string :deal_type
      t.integer :quantity
      t.decimal :discount_amount

      t.timestamps
    end
  end
end
