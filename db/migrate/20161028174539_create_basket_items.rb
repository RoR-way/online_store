class CreateBasketItems < ActiveRecord::Migration[5.0]
  def change
    create_table :basket_items do |t|
      t.integer :basket_id
      t.integer :quantity
      t.decimal :price
      t.integer :vat_rate_id
      t.integer :product_id

      t.timestamps
    end
  end
end
