Product.create(name: "Scotland Flag", price: 20.0, vat_rate_id: 1, deal_id: 1)
Product.create(name: "Children's Car Seat", price: 33.9, vat_rate_id: 2)
Product.create(name: "Magnetic Wrist Band", price: 9.0, vat_rate_id: 3)

Deal.create(deal_type: "item", quantity: 2, discount_amount: 5)
Deal.create(deal_type: "total", overall: 70, discount_rate: 12)

VatRate.create(name: "Zero", rate: 0)
VatRate.create(name: "Reduced", rate: 5)
VatRate.create(name: "Standard", rate: 20)