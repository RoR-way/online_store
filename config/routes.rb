Rails.application.routes.draw do
  root 'products#index'
  resources :products
  resources :baskets
  resources :basket_items
end
